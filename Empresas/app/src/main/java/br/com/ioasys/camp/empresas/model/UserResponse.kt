package br.com.ioasys.camp.empresas.model

import java.io.Serializable

data class UserResponse(
    var access_token: String,
    var client: String,
    var uid: String
) : Serializable