package br.com.ioasys.camp.empresas.retrofit

import br.com.ioasys.camp.empresas.model.UserResponse
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query

interface InterfaceRetrofit {
    @POST("api/v1/users/auth/sign_in")
    fun postUser(@Query("email") email: String, @Query("password") password: String) : Call<UserResponse>
}