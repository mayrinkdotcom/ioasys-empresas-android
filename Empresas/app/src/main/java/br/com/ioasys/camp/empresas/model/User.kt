package br.com.ioasys.camp.empresas.model

data class User(
    private var email: String,
    private var password: String
){
    fun setEmail(input: String){
        this.email = input
    }

    fun setPassword(input: String){
        this.password = input
    }
}