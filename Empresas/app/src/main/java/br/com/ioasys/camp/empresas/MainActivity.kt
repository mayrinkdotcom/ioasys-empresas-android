package br.com.ioasys.camp.empresas

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import br.com.ioasys.camp.empresas.model.UserResponse
import br.com.ioasys.camp.empresas.retrofit.Service
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_search.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnLogin.setOnClickListener {
            userLogin()
        }
    }
    private fun userLogin() {
        val user = UserResponse("","","")

        Service.retrofit.postUser(email.text.toString(), password.text.toString())
            .enqueue(object: Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    Log.d("Erro", t.toString())
                }

                override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                    Log.d("Sucesso", response.headers().toString())
                    response.headers().let {usuario ->
                        user.access_token = usuario.get("access-token")!!
                        user.client = usuario.get("client")!!
                        user.uid = usuario.get("uid")!!
                    }
                    setContentView(R.layout.activity_home)
                    btnSearch.setOnClickListener{
                        setContentView(R.layout.activity_search)
                        btnClose.setOnClickListener {
                            setContentView(R.layout.activity_home)
                        }
                    }
                }

            })
    }
}
