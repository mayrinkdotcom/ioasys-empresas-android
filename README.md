![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo expor os modelos de utilização e informações importantes acerca do desenvolvimento do projeto Empresas.

### ESCOPO DO PROJETO ###

* Deve ser criado um aplicativo Android utilizando linguagem Java ou Kotlin com as seguintes especificações:
* Login e acesso de Usuário já registrado
	* Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers (access-token, client, uid);
	* Para ter acesso as demais APIS precisamos enviar esses 3 custom headers para a API autorizar a requisição;
* Listagem de Empresas
* Detalhamento de Empresas

### Bibliotecas utilizadas como dependências ###

* GSON - Implementada para fazer o parse do JSON retornado pela API do Empresas, e buscar os dados necessários nos custom headers.
* OkHTTP - Interceptador de requests e responses HTTP, utilizado no processo de depuração do código, para verificar os dados que eram enviados à API e os seus retornos, assim como códigos de erro.
* Retrofit - Biblioteca de comunicação com a API, com a qual foi possível mapear, através de uma Interface HTTP, de que forma se daria a requisição dos Web Services, e a alocação das respostas da API.


### Informações Importantes ###

* Layout e recortes disponíveis no Zeplin (http://zeplin.io)
Login - teste_ioasys
Senha - ioasys123

* Integração disponível a partir de uma collection para Postman (https://www.getpostman.com/apps) disponível neste repositório.
* Projeto ainda em desenvolvimento.

### Release atual ###

O projeto dispõe de uma tela de login, na qual é necessário fornecer um endereço de e-mail e senha.
Após inseri-los e clocar em "Entrar", o App busca na API se as credenciais são válidas, liberando ou não o acesso.
Uma vez inseridas as credenciais corretas, o usuário será direcionado a uma tela *Home*, onde dispõe de uma tela inicialmente vazia.


### Futuras releases ###

Quando pressionado o botão de busca, ele exibirá um campo digitável, no qual o usuário deve fazer a pesquisa pela empresa que deseja conferir.
Dessa forma, o APP buscará na API as empresas que correspondem à busca, e mostrará um card contendo seu nome na tela para o usuário.
Ao clicar no card da empresa de seu interesse, o usuário será direcionado para outra tela, na qual irá conferir um resumo de informações sobre essa empresa específica.
Essa tela também conterá um botão para possibilitar voltar à tela de busca.

### Dados para Teste ###

* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

